<?php
$params = require __DIR__.'/params.php';
$db = require __DIR__.'/test_db.php';

/**
 * Application configuration shared by all test types
 */
return [
    'id'           => 'basic-tests',
    'basePath'     => dirname(__DIR__),
    'aliases'      => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'defaultRoute' => 'users',
    'bootstrap'    => [
        \app\bootstrap\EventDispatcherBootstrap::class,
    ],
    'language'     => 'en-US',
    'components'   => [
        'db'           => $db,
        'mailer'       => [
            'useFileTransport' => true,
        ],
        'assetManager' => [
            'basePath' => __DIR__.'/../web/assets',
        ],
        'urlManager'   => [
            'showScriptName' => true,
            'rules'          => require(__DIR__.DIRECTORY_SEPARATOR.'urlRules.php'),
        ],
        'user'         => [
            'identityClass' => app\models\Identity::class,
            'loginUrl'      => '/auth/login',
        ],
        'request'      => [
            'cookieValidationKey'  => 'test',
            'enableCsrfValidation' => false,
            // but if you absolutely need it set cookie domain to localhost
            /*
            'csrfCookie' => [
                'domain' => 'localhost',
            ],
            */
        ],
    ],
    'params'       => $params,
    'container'    => require(__DIR__.DIRECTORY_SEPARATOR.'dic.php'),
];
