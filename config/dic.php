<?php
$p = require(__DIR__.'/params.php');
return [
    'singletons' => [

        /**
         * EventDispatcher
         */
        \Symfony\Component\EventDispatcher\EventDispatcherInterface::class => [
            'class' => \Symfony\Component\EventDispatcher\EventDispatcher::class,
        ],

    ],

    'definitions' => [

    ],
];