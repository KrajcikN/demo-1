<?php

return [
    'adminEmail'      => 'admin@example.com',
    'user.rememberMe' => 60 * 60 * 24,
];
