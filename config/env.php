<?php

$dotenv = new Dotenv\Dotenv(__DIR__.'/..');
$dotenv->load();

$dotenv->required('YII_DEBUG');
$dotenv->required('YII_ENV');
$dotenv->required('YII_BASE_PATH');
$dotenv->required(['APP_ADMIN_EMAIL']);
$dotenv->required(['APP_LANGUAGES']);
$dotenv->required(['DATABASE_DSN', 'DATABASE_USER', 'DATABASE_PASSWORD']);