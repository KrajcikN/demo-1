<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.10.2017
 * Time: 7:59
 */

namespace app\application\guards;


use app\application\entities\TransferException;

class TransferGuard
{
    public static function checkSum($sum): void
    {
        if ($sum <= 0) {
            throw new TransferException('The transfer amount must be a positive integer.');
        }
    }

    public static function checkUsers($from, $to): void
    {
        if ($from === $to) {
            throw new TransferException('The sender and the receiver can not be the same.');
        }
    }
}