<?php

namespace app\application\forms;

use yii\base\Model;

class SignupLoginForm extends Model
{
    public $username;
    public $rememberMe;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['rememberMe', 'boolean'],
        ];
    }
}