<?php

namespace app\application\forms;

use app\application\entities\PaymentHistory;
use app\application\entities\User;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class PaymentHistorySearch extends Model
{
    public $username;
    public $transfer_amount;
    public $date_from;
    public $date_to;
    public $status;

    private $self_id = null;

    public function rules()
    {
        return [
            [['username'], 'string', 'length' => [2, 255]],
            [['status'], 'integer'],
            [['date_from', 'date_to'], 'date', 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @param array $params
     *
     * @param int   $id
     *
     * @return ActiveDataProvider
     */
    public function search(array $params, int $id): ActiveDataProvider
    {
        $this->self_id = $id;
        $query = PaymentHistory::find()
            ->with('sender', 'receiver')
            ->andWhere(
                [
                    'or',
                    ['from' => $id],
                    ['to' => $id],
                ]
            );
        $dataProvider = new ActiveDataProvider(
            [
                'query' => $query,
            ]
        );
        $this->load($params);
        if (!$this->validate()) {
            $query->where('0=1');

            return $dataProvider;
        }

        if (!empty($this->username)) {
            $userList = User::find()
                ->select(['id'])
                ->andWhere(['like', 'username', $this->username])
                ->column();

            $query
                ->andWhere(
                    [
                        'or',
                        [
                            'and',
                            ['from' => $userList],
                            ['to' => $id],
                        ],
                        [
                            'and',
                            ['from' => $id],
                            ['to' => $userList],
                        ],
                    ]
                );
        }

        if (!empty($this->status)) {
            switch ($this->status) {
                case PaymentHistory::SENT:
                    $query->andWhere(['from' => $id]);
                    break;
                case PaymentHistory::RECEIVED:
                    $query->andWhere(['to' => $id]);
                    break;
            }
        }

        $query
            ->andFilterWhere(['>=', 'created_at', $this->date_from ? strtotime($this->date_from.' 00:00:00') : null])
            ->andFilterWhere(['<=', 'created_at', $this->date_to ? strtotime($this->date_to.' 23:59:59') : null]);

        return $dataProvider;
    }

    public function getSelfId()
    {
        return $this->self_id;
    }
}