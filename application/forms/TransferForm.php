<?php

namespace app\application\forms;

use yii\base\Model;

class TransferForm extends Model
{
    public $username;
    public $transfer_amount;

    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            ['transfer_amount', 'required'],
            [
                'transfer_amount',
                'number',
                'numberPattern' => '/^\s*(?=.*[1-9])\d*(?:\.\d{1,2})?\s*$/',
                'message'       => 'The transfer amount must be positive, a maximum of 2 decimal places.',
            ],
        ];
    }

    public function attributeLabels()
    {
        return [
            'username'        => 'Receiver name',
            'transfer_amount' => 'Transfer amount',
        ];
    }
}