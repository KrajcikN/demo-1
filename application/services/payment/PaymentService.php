<?php

namespace app\application\services\payment;


use app\application\entities\User;
use app\application\Events;
use app\application\repositories\PaymentHistoryRepository;
use app\application\repositories\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class PaymentService
{
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;
    /**
     * @var PaymentHistoryRepository
     */
    private $historyRepository;

    /**
     * PaymentService constructor.
     *
     * @param UserRepository           $users
     * @param EventDispatcherInterface $dispatcher
     * @param PaymentHistoryRepository $historyRepository
     */
    public function __construct(
        UserRepository $users,
        EventDispatcherInterface $dispatcher,
        PaymentHistoryRepository $historyRepository
    ) {
        $this->users = $users;
        $this->dispatcher = $dispatcher;
        $this->historyRepository = $historyRepository;
    }

    /**
     * @param string $fromUsername
     * @param string $toUsername
     * @param int    $sum
     *
     * @throws \Exception
     */
    public function transfer(string $fromUsername, string $toUsername, int $sum): void
    {
        $transaction = User::getDb()->beginTransaction();
        try {
            $from = $this->getUser($fromUsername);
            $to = $this->getUser($toUsername);
            $from->transfer($to, $sum);
            $this->users->save($from);
            $this->users->save($to);
            $this->historyRepository->save($from->id, $to->id, $sum);
            $transaction->commit();
        } catch (\Exception $exception) {
            $transaction->rollBack();
            throw $exception;
        }
    }

    /**
     * @param string $username
     *
     * @return User
     */
    private function getUser(string $username): User
    {
        $user = $this->users->findByUsername($username);

        if (null === $user) {
            $user = User::signUp($username);
            $user->save();
            //todo отложенный вызов событий
            $this->dispatcher->dispatch(
                Events::EVENT_USER_CREATED,
                new GenericEvent($user)
            );
        }

        return $user;
    }
}