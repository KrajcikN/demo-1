<?php

namespace app\application\services\auth;


use app\application\entities\User;
use Throwable;

class UserAlreadyExistsException extends \DomainException
{

    /**
     * @var User
     */
    private $user;

    /**
     * UserAlreadyExistsException constructor.
     *
     * @param User           $user
     * @param string         $message
     * @param int            $code
     * @param Throwable|null $previous
     */
    public function __construct(User $user, $message = "", $code = 0, Throwable $previous = null)
    {
        $this->user = $user;
        parent::__construct($message, $code, $previous);
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

}