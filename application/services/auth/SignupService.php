<?php

namespace app\application\services\auth;


use app\application\entities\User;
use app\application\Events;
use app\application\forms\SignupLoginForm;
use app\application\repositories\UserRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\GenericEvent;

class SignupService
{
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var EventDispatcherInterface
     */
    private $dispatcher;

    /**
     * SignupService constructor.
     *
     * @param UserRepository           $users
     * @param EventDispatcherInterface $dispatcher
     */
    public function __construct(UserRepository $users, EventDispatcherInterface $dispatcher)
    {
        $this->users = $users;
        $this->dispatcher = $dispatcher;
    }

    /**
     * @param SignupLoginForm $form
     *
     * @return User
     */
    public function signup(SignupLoginForm $form): User
    {
        if (null !== $user = $this->users->findByUsername($form->username)) {
            throw new UserAlreadyExistsException($user);
        }

        $user = User::signUp($form->username);
        $this->users->save($user);
        $this->dispatcher->dispatch(
            Events::EVENT_USER_CREATED,
            (new GenericEvent($user))
        );

        return $user;
    }
}