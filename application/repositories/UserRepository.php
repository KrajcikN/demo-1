<?php

namespace app\application\repositories;


use app\application\entities\User;

class UserRepository
{

    /**
     * @param $id
     *
     * @return User|null
     */
    public function find($id): ?User
    {
        return User::find()
            ->andWhere(['id' => $id])
            ->limit(1)
            ->one();
    }

    /**
     * @param string $value
     *
     * @return User|null
     */
    public function findByUsername(string $value): ?User
    {
        return User::find()
            ->andWhere(
                "LOWER(username) = LOWER(:value)",
                ['value' => $value]
            )
            ->limit(1)
            ->one();
    }

    public function getAll(): array
    {
        return User::find()->all();
    }

    /**
     * @param User $user
     *
     * @throws \RuntimeException
     */
    public function save(User $user): void
    {
        if (false === $user->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }


}