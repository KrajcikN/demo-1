<?php

namespace app\application\repositories;


use app\application\entities\PaymentHistory;

class PaymentHistoryRepository
{
    /**
     * @param int $from
     * @param int $to
     * @param int $sum
     */
    public function save(int $from, int $to, int $sum): void
    {
        \Yii::$app->db->createCommand()->insert(
            '{{%transfers}}',
            [
                'from'            => $from,
                'to'              => $to,
                'transfer_amount' => $sum,
                'created_at'      => time(),
            ]
        )->execute();
    }

    public function exists(int $from, int $to, int $sum, int $timestamp): bool
    {
        return PaymentHistory::find()
            ->andWhere(['from' => $from])
            ->andWhere(['to' => $to])
            ->andWhere(['transfer_amount' => $sum])
            ->andWhere(['created_at' => $timestamp])
            ->exists();
    }

    /**
     * @param int $id
     *
     * @return array
     */
    public function getAllByUser(int $id): array
    {
        return PaymentHistory::find()
            ->select(['from', 'to', 'transfer_amount', 'created_at'])
            ->andWhere(['from' => $id])
            ->orWhere(['to' => $id])
            ->all();
    }
}