<?php

namespace app\application\entities;

/**
 * This is the model class for table "{{%transfers}}".
 *
 * @property integer $id
 * @property integer $from
 * @property integer $to
 * @property integer $transfer_amount
 * @property integer $created_at
 *
 * @property User    $sender
 * @property User    $receiver
 */
class PaymentHistory extends \yii\db\ActiveRecord
{
    const SENT = 1;
    const RECEIVED = 2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%transfers}}';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSender()
    {
        return $this->hasOne(User::className(), ['id' => 'from']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceiver()
    {
        return $this->hasOne(User::className(), ['id' => 'to']);
    }

}
