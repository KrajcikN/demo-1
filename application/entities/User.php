<?php

namespace app\application\entities;

use app\application\guards\TransferGuard;
use yii\base\Security;
use yii\db\ActiveRecord;

/**
 * User entity
 *
 * @property  integer $id
 * @property  string  $auth_key
 * @property  string  $username
 * @property  integer $balance
 * @property  integer $created_at
 */
class User extends ActiveRecord
{
    /**
     * @param string $username
     *
     * @return User
     */
    public static function signUp(string $username): self
    {
        $user = new self();
        $user->username = $username;
        $user->auth_key = (new Security)->generateRandomString();
        $user->balance = 0;
        $user->created_at = time();

        return $user;
    }

    /**
     * @param User $to
     * @param int  $sum
     *
     * @throws TransferException
     */
    public function transfer(User $to, int $sum): void
    {
        TransferGuard::checkSum($sum);
        TransferGuard::checkUsers($this->id, $to->id);
        $this->balance -= $sum;
        $to->balance += $sum;
    }
}
