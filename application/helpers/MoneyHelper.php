<?php

namespace app\application\helpers;


use app\application\entities\PaymentHistory;

class MoneyHelper
{
    /**
     * @param int $balance
     *
     * @return float
     */
    public static function balanceForHumans(int $balance): float
    {
        return $balance / 100;
    }

    /**
     * @param float $balance
     *
     * @return int
     */
    public static function balanceForEntity(float $balance): int
    {
        return round($balance * 100);
    }

    public static function getPaymentDirectionList()
    {
        return [
            PaymentHistory::SENT     => 'Sent to',
            PaymentHistory::RECEIVED => 'Received from',
        ];
    }
}