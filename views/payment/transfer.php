<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model \app\application\forms\TransferForm */

use kartik\money\MaskMoney;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

$this->title = 'Transfer funds to another user';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-transfer">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>Please fill out the following fields:</p>

    <?php $form = ActiveForm::begin(
        [
            'id'     => 'transfer-form',
            'layout' => 'horizontal',
        ]
    ); ?>

    <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

    <?= $form->field($model, 'transfer_amount')->widget(
        MaskMoney::class,
        [
            'pluginOptions' => [
                'prefix'           => '$ ',
                'allowNegative'    => false,
                'displayInputName' => false,
            ],
        ]
    ); ?>

    <div class="form-group">
        <div class="col-lg-offset-3 col-lg-11">
            <?= Html::submitButton('Transfer funds', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
