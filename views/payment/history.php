<?php

use app\application\entities\PaymentHistory;
use app\application\helpers\MoneyHelper;
use kartik\date\DatePicker;
use yii\grid\GridView;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel \app\application\forms\PaymentHistorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Payment history';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-history">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'rowOptions'   => function (PaymentHistory $model) use ($searchModel) {
                $c = $searchModel->getSelfId() == $model->from
                    ? 'info' : 'success';

                return ['class' => $c];
            },
            'columns'      => [
                [
                    'label'     => 'Direction of payment',
                    'attribute' => 'status',
                    'filter'    => MoneyHelper::getPaymentDirectionList(),
                    'value'     => function (PaymentHistory $model) use ($searchModel) {
                        return $searchModel->getSelfId() == $model->from ?
                            'Sent to' : 'Received from';
                    },

                    'format' => 'html',
                ],
                [
                    'label'     => 'Username',
                    'attribute' => 'username',
                    'value'     => function (PaymentHistory $model) use ($searchModel) {
                        return $searchModel->getSelfId() == $model->from ?
                            Html::encode($model->receiver->username) : Html::encode($model->sender->username);
                    },

                    'format' => 'html',
                ],
                [
                    'label'     => 'Date time',
                    'attribute' => 'created_at',
                    'filter'    => DatePicker::widget(
                        [
                            'model'         => $searchModel,
                            'attribute'     => 'date_from',
                            'attribute2'    => 'date_to',
                            'type'          => DatePicker::TYPE_RANGE,
                            'separator'     => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose'      => true,
                                'format'         => 'yyyy-mm-dd',
                            ],
                        ]
                    ),
                    'format'    => 'datetime',
                ],
                [
                    'attribute' => 'transfer_amount',
                    'value'     => function (PaymentHistory $model) {
                        return MoneyHelper::balanceForHumans($model->transfer_amount);
                    },
                    'format'    => 'currency',
                ],
            ],
        ]
    ); ?>
</div>