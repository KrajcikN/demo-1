<?php

use app\application\entities\User;
use app\application\helpers\MoneyHelper;
use kartik\date\DatePicker;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel \app\application\forms\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Users';
?>
<div class="user-index">
    <?= GridView::widget(
        [
            'dataProvider' => $dataProvider,
            'filterModel'  => $searchModel,
            'columns'      => [
                'username',
                [
                    'label'     => 'Signup date time',
                    'attribute' => 'created_at',
                    'filter'    => DatePicker::widget(
                        [
                            'model'         => $searchModel,
                            'attribute'     => 'date_from',
                            'attribute2'    => 'date_to',
                            'type'          => DatePicker::TYPE_RANGE,
                            'separator'     => '-',
                            'pluginOptions' => [
                                'todayHighlight' => true,
                                'autoclose'      => true,
                                'format'         => 'yyyy-mm-dd',
                            ],
                        ]
                    ),
                    'format'    => 'datetime',
                ],
                [
                    'attribute' => 'balance',
                    'value'     => function (User $model) {
                        return MoneyHelper::balanceForHumans($model->balance);
                    },
                    'format'    => 'currency',
                ],
            ],
        ]
    ); ?>
</div>