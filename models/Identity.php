<?php

namespace app\models;

use app\application\entities\User;
use app\application\repositories\UserRepository;
use yii\web\IdentityInterface;

class Identity implements IdentityInterface
{
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param int $id
     *
     * @return Identity|null
     */
    public static function findIdentity($id)
    {
        $user = self::getRepository()->find($id);

        return $user ? new self($user) : null;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->user->id;
    }

    public function getAuthKey(): string
    {
        return $this->user->auth_key;
    }

    public function validateAuthKey($authKey): bool
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getUsername()
    {
        return $this->user->username;
    }

    public function getBalance()
    {
        return $this->user->balance;
    }

    /**
     * @inheritDoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }


    /**
     * @return UserRepository
     */
    private static function getRepository(): UserRepository
    {
        return \Yii::$container->get(UserRepository::class);
    }
}