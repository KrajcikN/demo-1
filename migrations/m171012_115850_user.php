<?php

use yii\db\Migration;

class m171012_115850_user extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%user}}', [
            'id'       => $this->primaryKey(),
            'auth_key' => $this->string(32)->notNull(),
            'username' => $this->string()->notNull()->unique(),
            'balance'  => $this->bigInteger()->notNull()->defaultValue(0),

            'created_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%user}}');
    }
}
