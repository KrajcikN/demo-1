<?php

use yii\db\Migration;

/**
 * Handles the creation of table `transfers`.
 */
class m171016_223233_create_transfers_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable(
            '{{%transfers}}',
            [
                'id'              => $this->primaryKey(),
                'from'            => $this->integer()->notNull(),
                'to'              => $this->integer()->notNull(),
                'transfer_amount' => $this->bigInteger()->notNull(),
                'created_at'      => $this->integer()->notNull(),
            ]
        );
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('{{%transfers}}');
    }
}
