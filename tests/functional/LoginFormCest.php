<?php

class LoginFormCest
{
    public function _before(\FunctionalTester $I)
    {
        $I->haveFixtures(
            [
                'user' => [
                    'class'    => \app\fixtures\UserFixture::className(),
                    'dataFile' => codecept_data_dir().'user.php',
                ],
            ]
        );

        $I->amOnRoute('auth/login');
    }

    public function openLoginPage(\FunctionalTester $I)
    {
        $I->see('Login', 'h1');

    }

    // demonstrates `amLoggedInAs` method
    public function internalLoginById(\FunctionalTester $I)
    {
        $I->amLoggedInAs(2);
        $I->amOnPage('/');
        $I->see('Logout (xwindler)');
    }

    // demonstrates `amLoggedInAs` method
    public function internalLoginByInstance(\FunctionalTester $I)
    {
        $I->amLoggedInAs(
            new \app\models\Identity(
                (new \app\application\repositories\UserRepository)
                    ->findByUsername('xwindler')
            )
        );
        $I->amOnPage('/');
        $I->see('Logout (xwindler)');
    }

    public function loginWithEmptyCredentials(\FunctionalTester $I)
    {
        $I->submitForm('#login-form', []);
        $I->expectTo('see validations errors');
        $I->see('Username cannot be blank.');
    }

    public function loginSuccessfully(\FunctionalTester $I)
    {
        $I->submitForm(
            '#login-form',
            [
                'SignupLoginForm[username]' => 'xwindler',
            ]
        );
        $I->see('Logout (xwindler)');
        $I->dontSeeElement('form#login-form');
    }
}