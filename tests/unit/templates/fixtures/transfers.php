<?php
return [
    'id'              => $faker->unique()->numberBetween(1, 30),
    'from'            => $faker->numberBetween(1, 6),
    'to'              => $faker->numberBetween(1, 6),
    'transfer_amount' => $faker->randomNumber(),
    'created_at'      => $faker->dateTimeThisYear->getTimestamp(),
];
