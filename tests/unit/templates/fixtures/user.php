<?php
return [
    'id' => $faker->unique()->numberBetween(1,6),
    'username' => $faker->userName,
    'balance' => $faker->randomNumber(),
    'created_at' => $faker->dateTimeThisYear->getTimestamp(),
];
