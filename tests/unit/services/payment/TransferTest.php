<?php

namespace tests\services\auth;

use app\application\entities\TransferException;
use app\application\repositories\PaymentHistoryRepository;
use app\application\repositories\UserRepository;
use app\application\services\payment\PaymentService;
use app\fixtures\UserFixture;
use Codeception\Test\Unit;
use Symfony\Component\EventDispatcher\EventDispatcher;

class TransferTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;
    /**
     * @var PaymentService
     */
    private $paymentService;
    /**
     * @var UserRepository
     */
    private $users;
    /**
     * @var PaymentHistoryRepository
     */
    private $historyRepository;

    public function _before()
    {
        $this->tester->haveFixtures(
            [
                'user' => [
                    'class'    => UserFixture::className(),
                    'dataFile' => codecept_data_dir().'user.php',
                ],
            ]
        );
        $this->users = new UserRepository();
        $this->paymentService = new PaymentService(
            new UserRepository(),
            new EventDispatcher(),
            new PaymentHistoryRepository()
        );
        $this->historyRepository = new PaymentHistoryRepository();
    }

    public function testSuccess()
    {
        $begunBalanceFrom = $this->users->findByUsername($fromUsername = 'mlowe')->balance;
        $begunBalanceTo = $this->users->findByUsername($toUsername = 'mbeatty')->balance;
        $this->paymentService->transfer(
            $fromUsername,
            $toUsername,
            $sum = 99999
        );
        $from = $this->users->findByUsername($fromUsername);
        $to = $this->users->findByUsername($toUsername);

        expect($from->balance)->equals($begunBalanceFrom - $sum);
        expect($to->balance)->equals($begunBalanceTo + $sum);
        expect_that($this->historyRepository->exists($from->id, $to->id, $sum, time()));
    }

    public function testToNewUserSuccess()
    {
        $toUsername = 'unregistered_user';
        expect_not($this->users->findByUsername($toUsername));

        $begunBalanceFrom = $this->users->findByUsername($fromUsername = 'mlowe')->balance;
        $this->paymentService->transfer(
            $fromUsername,
            $toUsername,
            $sum = 99999
        );
        $from = $this->users->findByUsername($fromUsername);
        $to = $this->users->findByUsername($toUsername);

        expect($to)->notNull();
        expect($from->balance)->equals($begunBalanceFrom - $sum);
        expect($to->balance)->equals($sum);
        expect_that($this->historyRepository->exists($from->id, $to->id, $sum, time()));
    }

    public function testRollBack()
    {
        $this->expectException(TransferException::class);
        $this->expectExceptionMessage('The sender and the receiver can not be the same.');

        $fromUsername = 'unregistered_user';
        $toUsername = 'unregistered_user';
        expect_not($this->users->findByUsername($toUsername));

        $this->paymentService->transfer(
            $fromUsername,
            $toUsername,
            $sum = 99999
        );
        $from = $this->users->findByUsername($fromUsername);
        $to = $this->users->findByUsername($toUsername);
        expect_not($from);
        expect_not($to);
    }
}