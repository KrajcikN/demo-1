<?php

namespace tests\services\auth;

use app\application\forms\SignupLoginForm;
use app\application\repositories\UserRepository;
use app\application\services\auth\SignupService;
use app\application\services\auth\UserAlreadyExistsException;
use app\fixtures\UserFixture;
use Codeception\Test\Unit;
use Symfony\Component\EventDispatcher\EventDispatcher;

class SignupTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var SignupService
     */
    private $service;

    /**
     * @var SignupLoginForm
     */
    private $form;

    public function _before()
    {
        $this->tester->haveFixtures(
            [
                'user' => [
                    'class'    => UserFixture::className(),
                    'dataFile' => codecept_data_dir().'user.php',
                ],
            ]
        );

        $this->service = new SignupService(
            new UserRepository(),
            new EventDispatcher()
        );
        $this->form = new SignupLoginForm();
    }

    public function testSuccess()
    {
        $this->form->username = $username = 'test_username';
        $this->service->signup($this->form);

        $user = (new UserRepository())->findByUsername($username);

        expect_that(null !== $user);
        expect('user id = 7', $user->id)->equals(7);
        expect('username is correct', $user->username)->equals($username);
        expect('user balance equals 0', $user->balance)->equals(0);
        expect('created time is correct', time() - $user->created_at)->lessThan(10);
    }

    public function testUserAlreadyExists()
    {
        $this->expectException(UserAlreadyExistsException::class);

        $this->form->username = $username = 'alysa.nikolaus';
        $this->service->signup($this->form);
    }
}