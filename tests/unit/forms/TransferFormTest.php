<?php

namespace tests\forms;

use app\application\forms\TransferForm;
use Codeception\Test\Unit;

class TransferFormTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    private $message = 'The transfer amount must be positive, a maximum of 2 decimal places.';
    /**
     * @var TransferForm
     */
    private $form;

    public function _before()
    {
        $this->form = new TransferForm();
    }

    public function testCorrectValidate()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = 50.45;
        expect_that($this->form->validate());
    }

    public function testEmptyUsername()
    {
        $this->form->username = '';
        $this->form->transfer_amount = 0.54;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('username');
        expect($errors)->notEmpty();
        expect_that(
            in_array('Receiver name cannot be blank.', $errors)
        );
    }

    public function testShortUsername()
    {
        $this->form->username = 'a';
        $this->form->transfer_amount = 0.54;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('username');
        expect($errors)->notEmpty();
        expect_that(
            in_array('Receiver name should contain at least 2 characters.', $errors)
        );
    }

    public function testSum3DecimalPlaces()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = 50.1155;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('transfer_amount');
        expect($errors)->notEmpty();
        expect_that(
            in_array($this->message, $errors)
        );
    }

    public function testSumIsNull()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = 0;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('transfer_amount');
        expect($errors)->notEmpty();
        expect_that(
            in_array($this->message, $errors)
        );
    }

    public function testSumIsNegative()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = -55;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('transfer_amount');
        expect($errors)->notEmpty();
        expect_that(
            in_array($this->message, $errors)
        );
    }

    public function testSumString()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = 'string';
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('transfer_amount');
        expect($errors)->notEmpty();
        expect_that(
            in_array($this->message, $errors)
        );
    }

    public function testTransferYourself()
    {
        $this->form->username = 'test_username';
        $this->form->transfer_amount = 50.1155;
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('transfer_amount');
        expect($errors)->notEmpty();
        expect_that(
            in_array($this->message, $errors)
        );
    }
}