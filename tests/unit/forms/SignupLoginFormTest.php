<?php
namespace tests\forms;
use app\application\forms\SignupLoginForm;
use Codeception\Test\Unit;

class SignupLoginFormTest extends Unit
{
    /**
     * @var SignupLoginForm
     */
    private $form;
    public function _before()
    {
        $this->form = new SignupLoginForm();
    }

    public function testCorrectValidate()
    {
        $this->form->username = 'test_username';
        expect_that($this->form->validate());
    }

    public function testEmptyUsername()
    {
        $this->form->username = '';
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('username');
        expect($errors)->notEmpty();
        expect_that(
            in_array('Username cannot be blank.', $errors)
        );
    }

    public function testShortUsername()
    {
        $this->form->username = 'a';
        expect_not($this->form->validate());

        $errors = $this->form->getErrors('username');
        expect($errors)->notEmpty();
        expect_that(
            in_array('Username should contain at least 2 characters.', $errors)
        );
    }
}