<?php

namespace tests\entities\Payment;

use app\application\repositories\PaymentHistoryRepository;
use app\application\repositories\UserRepository;
use app\fixtures\PaymentHistoryFixture;
use app\fixtures\UserFixture;
use Codeception\Test\Unit;

//todo GET tests
class PaymentHistoryRepositoryTest extends Unit
{
    /**
     * @var UserRepository
     */
    private $users;

    /**
     * @var PaymentHistoryRepository
     */
    private $payments;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures(
            [
                'user'      => [
                    'class'    => UserFixture::class,
                    'dataFile' => codecept_data_dir().'user.php',
                ],
                'transfers' => [
                    'class'    => PaymentHistoryFixture::class,
                    'dataFile' => codecept_data_dir().'transfers.php',
                ],
            ]
        );
        $this->users = new UserRepository();
        $this->payments = new PaymentHistoryRepository();
    }

    public function testExistsSuccess()
    {
        $transfer = $this->tester->grabFixture('transfers', 0);
        expect_that(
            $this->payments->exists(
                $transfer['from'],
                $transfer['to'],
                $transfer['transfer_amount'],
                $transfer['created_at']
            )
        );
    }

    public function testExistsFail()
    {
        expect_not(
            $this->payments->exists(
                999,
                1001,
                5,
                123
            )
        );
    }
}