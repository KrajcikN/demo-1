<?php

namespace tests\entities\User;

use app\application\entities\User;
use Codeception\Test\Unit;

class SignupTest extends Unit
{
    public function testSuccess()
    {
        $user = User::signUp(
            $username = 'username'
        );

        $this->assertInstanceOf(User::class, $user);
        $this->assertEquals(32, strlen($user->auth_key));
        $this->assertEquals($username, $user->username);
        $this->assertEquals(0, $user->balance);
        $this->assertNotEmpty($user->created_at);
    }
}