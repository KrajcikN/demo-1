<?php

namespace tests\entities\User;

use app\application\entities\TransferException;
use app\application\repositories\UserRepository;
use app\fixtures\UserFixture;
use Codeception\Test\Unit;

class TransferTest extends Unit
{
    /**
     * @var UserRepository
     */
    public $users;

    /**
     * @var \UnitTester
     */
    protected $tester;

    public function _before()
    {
        $this->tester->haveFixtures(
            [
                'user' => [
                    'class'    => UserFixture::className(),
                    'dataFile' => codecept_data_dir().'user.php',
                ],
            ]
        );
        $this->users = new UserRepository();
    }

    public function testSuccess()
    {
        $from = $this->users->find(1);
        $fromBegunBalance = $from->balance;

        $to = $this->users->find(2);
        $toBegunBalance = $to->balance;

        $from->transfer($to, $transferSum = 50000);
        expect($from->balance)->equals($fromBegunBalance - $transferSum);
        expect($to->balance)->equals($toBegunBalance + $transferSum);
    }

    public function testNegativeTransferSum()
    {
        $from = $this->users->find(1);
        $to = $this->users->find(2);

        $this->expectException(TransferException::class);
        $this->expectExceptionMessage('The transfer amount must be a positive integer.');
        $from->transfer($to, $transferSum = -500);
    }

    public function testNullTransferSum()
    {
        $from = $this->users->find(1);
        $to = $this->users->find(2);

        $this->expectException(TransferException::class);
        $this->expectExceptionMessage('The transfer amount must be a positive integer.');
        $from->transfer($to, $transferSum = 0);
    }

    public function testCompareUsers()
    {
        $from = $this->users->find(1);
        $to = $this->users->find(1);

        $this->expectException(TransferException::class);
        $this->expectExceptionMessage('The sender and the receiver can not be the same.');
        $from->transfer($to, $transferSum = 15);
    }
}