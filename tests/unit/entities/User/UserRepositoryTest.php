<?php

namespace tests\entities\User;

use app\application\entities\User;
use app\application\repositories\UserRepository;
use app\fixtures\UserFixture;
use app\models\Identity;
use Codeception\Test\Unit;

class UserRepositoryTest extends Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    /**
     * @var UserRepository
     */
    private $users;

    public function _before()
    {
        $this->tester->haveFixtures(
            [
                'user' => [
                    'class'    => UserFixture::className(),
                    'dataFile' => codecept_data_dir().'user.php',
                ],
            ]
        );
        $this->users = new UserRepository();
    }

    public function testFindUserById()
    {
        expect_that($user = $this->users->find(1));
        expect($user->username)->equals('alysa.nikolaus');

        expect_not($this->users->find(999));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = $this->users->findByUsername('mlowe'));
        expect_not($this->users->findByUsername('not-admin'));
        expect(
            'search by username is not case sensitive',
            ($this->users->findByUsername('MlowE'))->id
        )->equals($user->id);
    }

    public function testGetAll()
    {
        expect($users = $this->users->getAll())->notEmpty();
        expect($users[0])->isInstanceOf(User::class);
        expect($users)->count(6);
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testIdentityValidateUser()
    {
        $user = $this->users->findByUsername('mlowe');
        $identity = new Identity($user);
        expect_that($identity->validateAuthKey('1AvCpExYPHCwZ9FWTZVeSXu5fNtg7W1E'));
        expect_not($identity->validateAuthKey('test102key'));
    }

}
