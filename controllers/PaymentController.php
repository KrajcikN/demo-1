<?php

namespace app\controllers;


use app\application\entities\TransferException;
use app\application\forms\PaymentHistorySearch;
use app\application\forms\TransferForm;
use app\application\helpers\MoneyHelper;
use app\application\repositories\UserRepository;
use app\application\services\payment\PaymentService;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;

class PaymentController extends Controller
{
    public $defaultAction = 'history';
    /**
     * @var PaymentService
     */
    private $paymentService;
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * PaymentController constructor.
     *
     * @param string         $id
     * @param Module         $module
     * @param PaymentService $paymentService
     * @param UserRepository $userRepository
     * @param array          $config
     */
    public function __construct(
        $id,
        Module $module,
        PaymentService $paymentService,
        UserRepository $userRepository,
        array $config = []
    ) {
        parent::__construct($id, $module, $config);
        $this->paymentService = $paymentService;
        $this->userRepository = $userRepository;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }


    public function actionTransfer()
    {
        $form = new TransferForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $this->paymentService->transfer(
                    Yii::$app->user->getIdentity()->getUsername(),
                    $form->username,
                    MoneyHelper::balanceForEntity($form->transfer_amount)
                );
                Yii::$app->session->setFlash(
                    'success',
                    Yii::$app->formatter->asCurrency(
                        Html::encode($form->transfer_amount)
                    ).' was successfully transferred to '.Html::encode($form->username).'.'
                );
            } catch (TransferException $exception) {
                Yii::$app->session->setFlash('error', Html::encode($exception->getMessage()));

                return $this->refresh();
            } catch (\Exception $exception) {
                Yii::$app->errorHandler->logException($exception);
                throw $exception;
            }

            return $this->goHome();
        }

        return $this->render(
            'transfer',
            [
                'model' => $form,
            ]
        );
    }

    public function actionHistory()
    {
        $searchModel = new PaymentHistorySearch();
        $dataProvider = $searchModel->search(
            Yii::$app->request->queryParams,
            Yii::$app->user->id
        );

        return $this->render(
            'history',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}