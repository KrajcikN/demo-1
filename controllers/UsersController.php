<?php

namespace app\controllers;


use app\application\forms\UserSearch;
use yii\web\Controller;

class UsersController extends Controller
{
    public $defaultAction = 'list';

    public function actionList()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(\Yii::$app->request->queryParams);

        return $this->render(
            'list',
            [
                'searchModel'  => $searchModel,
                'dataProvider' => $dataProvider,
            ]
        );
    }
}