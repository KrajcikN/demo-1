<?php

namespace app\controllers;


use app\application\forms\SignupLoginForm;
use app\application\services\auth\SignupService;
use app\application\services\auth\UserAlreadyExistsException;
use app\models\Identity;
use Yii;
use yii\base\Module;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;

class AuthController extends Controller
{
    /**
     * @var SignupService
     */
    private $signup;

    /**
     * AuthController constructor.
     *
     * @param string        $id
     * @param Module        $module
     * @param SignupService $signup
     * @param array         $config
     */
    public function __construct($id, Module $module, SignupService $signup, array $config = [])
    {
        parent::__construct($id, $module, $config);
        $this->signup = $signup;
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only'  => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow'   => true,
                        'roles'   => ['@'],
                    ],
                ],
            ],
            'verbs'  => [
                'class'   => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $form = new SignupLoginForm();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            try {
                $user = $this->signup->signup($form);
            } catch (UserAlreadyExistsException $exception) {
                $user = $exception->getUser();
            } catch (\Exception $exception) {
                Yii::$app->errorHandler->logException($exception);

                return $this->refresh();
            }
            Yii::$app->user->login(
                new Identity($user),
                $form->rememberMe ? Yii::$app->params['user.rememberMe'] : 0
            );

            return $this->goBack();
        }

        return $this->render(
            'login',
            [
                'model' => $form,
            ]
        );
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}