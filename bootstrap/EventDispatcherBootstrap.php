<?php

namespace app\bootstrap;

use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use yii\base\BootstrapInterface;

/**
 * Инициализация и конфигурация event dispatcher
 * @link https://symfony.com/doc/current/components/event_dispatcher.html
 */
class EventDispatcherBootstrap implements BootstrapInterface
{
    protected $listeners;
    protected $subscribers;
    protected $dispatcher;

    public function __construct(EventDispatcherInterface $dispatcher)
    {
        $this->dispatcher = $dispatcher;
    }

    public function bootstrap($app)
    {
//        $this->dispatcher->addListener('acme.foo.action', function (Event $event) {
//        });

    }
}