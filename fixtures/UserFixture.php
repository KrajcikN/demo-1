<?php

namespace app\fixtures;

use app\application\entities\User;
use yii\test\ActiveFixture;

class UserFixture extends ActiveFixture
{
    public $modelClass = User::class;
}