<?php

namespace app\fixtures;


use app\application\entities\PaymentHistory;
use yii\test\ActiveFixture;

class PaymentHistoryFixture extends ActiveFixture
{
    public $modelClass = PaymentHistory::class;
}